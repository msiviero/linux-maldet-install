#!/bin/sh
cd /usr/local/src/
wget http://www.rfxn.com/downloads/maldetect-current.tar.gz
tar -xzvf maldetect-current.tar.gz
rm -rf maldetect-current.tar.gz
cd maldetect-*
sh install.sh
maldet -d && maldet -u
maldet -b --scan-all /
